package com.devcamp.restapi.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.Controller.models.Circle;
import com.devcamp.restapi.Controller.models.Cylinder;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CircleCylinderAPI {

  @GetMapping("/circle-area")
  public double getCircleArea(double radius) {
    Circle circle = new Circle(radius);
    return circle.getArea();
  }

  @GetMapping("/cylinder-volume")
  public double getCircleVolume(double radius, double height) {
    Cylinder cylinder = new Cylinder(radius, height);
    return cylinder.getVolume();
  }
}
